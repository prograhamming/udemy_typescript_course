/*
    TYPE BASICS
*/

//assign types
let myAge: number = 23;
let str: string = "test";
let hasBoolean: boolean = false;

//array
let hobbies: string[] = ["Cooking", "Sports"];

//tuples
let address: [string, number] = ["Superstreet", 90];

//enum
enum Color {
    Gray, //automatically receives the number 0
    Green = 100, //100 -> because you defined it.
    Blue //2
}

let myColor: Color = Color.Green;
console.log(myColor)

// any
let car: any = 23;

//Functions and types
function returnMyName(name: string): string {
    return name;
}

//argument types
function multiply(value1: number, value2: number): number {
    return value1 * value2;
}

//function types
let myMultiply: (val1: number, val2: number) => number; //function as a type
myMultiply = multiply;
myMultiply(5, 2);

//object and types
let userData: {name: string, age: number} = {
    name: "Max",
    age: 27
}; 

//advanced object
let complex: { data: number[], output: (all: boolean) => number[]} = {
    data: [100, 3.99, 10],
    output: function(all: boolean): number[] {
        return this.data;
    }
}

//Creating Custom Types with Type Aliases

    //type alias
type Complex = { data: number[], output: (all: boolean) => number[] }

let complex2: Complex = {
    data: [100, 3, 99, 10],
    output: function(all: boolean): number[] { 
        let num: number[] = [1, 2, 3];
        return num;
     }
}

//Allowing multiple Types with Union Types

    //union types
let myRealRealAge: number | string = 27;

//Checking for Types during Runtime
let finalValue = "A string";
if (typeof finalValue == "string") { //you have to use quotations around the type to check the type
    console.log("Final value is a string");
}

//The never Type
function neverReturns():never { //used to explicitly say you will never run this function in your code.
    throw new Error('An Error');
}

neverReturns();

//Nullable Types
let canBeNull: null = null; // you can change whether variables can be null in the ts.config file
canBeNull = null;


