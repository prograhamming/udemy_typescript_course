"use strict";
//let & const
//The let keyword creates a block scope variable. Which means it is contains within the block it is in.
var myVar = "test";
console.log("LINE 4 => " + myVar);
//Const
//When you define a variable with const the value cannot change initializing it. For example, tax rate, max level, etc.
var maxLevels = 99;
//Block Scope
function reset() {
    var myVar = 22;
    console.log("LINE 12 => " + myVar);
}
reset();
//Arrow Functions
var addNumbers = function (value1, value2) {
    return value1 + value2;
};
addNumbers(2, 3);
