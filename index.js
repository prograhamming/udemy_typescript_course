"use strict";
/*
    TYPE BASICS
*/
//assign types
var myAge = 23;
var str = "test";
var hasBoolean = false;
//array
var hobbies = ["Cooking", "Sports"];
//tuples
var address = ["Superstreet", 90];
//enum
var Color;
(function (Color) {
    Color[Color["Gray"] = 0] = "Gray";
    Color[Color["Green"] = 100] = "Green";
    Color[Color["Blue"] = 101] = "Blue"; //2
})(Color || (Color = {}));
var myColor = Color.Green;
console.log(myColor);
// any
var car = 23;
//Functions and types
function returnMyName(name) {
    return name;
}
//argument types
function multiply(value1, value2) {
    return value1 * value2;
}
//function types
var myMultiply; //function as a type
myMultiply = multiply;
myMultiply(5, 2);
//object and types
var userData = {
    name: "Max",
    age: 27
};
//advanced object
var complex = {
    data: [100, 3.99, 10],
    output: function (all) {
        return this.data;
    }
};
var complex2 = {
    data: [100, 3, 99, 10],
    output: function (all) {
        var num = [1, 2, 3];
        return num;
    }
};
//Allowing multiple Types with Union Types
//union types
var myRealRealAge = 27;
//Checking for Types during Runtime
var finalValue = "A string";
if (typeof finalValue == "string") { //you have to use quotations around the type to check the type
    console.log("Final value is a string");
}
//The never Type
function neverReturns() {
    throw new Error('An Error');
}
neverReturns();
//Nullable Types
var canBeNull = null; // you can change whether variables can be null in the ts.config file
canBeNull = null;
