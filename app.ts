//let & const
//The let keyword creates a block scope variable. Which means it is contains within the block it is in.
let myVar: string = "test";
console.log(`LINE 4 => ${myVar}`);

//Const
//When you define a variable with const the value cannot change initializing it. For example, tax rate, max level, etc.
const maxLevels = 99;

//Block Scope
function reset(): void {
    let myVar: number = 22;
    console.log(`LINE 12 => ${myVar}`);
}

reset();

//Arrow Functions
const addNumbers = (value1: number, value2: number): number => {
    return value1 + value2;
}

const greet = () => console.log("Hello");
const greetTim = (friend: string) => console.log(friend);

//default parameters
const sum = (val1: number = 2, val2: number = val1 - 5) => {
    return val1 + val2;
}

//spread & rest operators
    //-Whenever you use call something and pass something in JavaScript knows to use the spread operator
    /*-Whenever you pass a list of values to a function and you use the rest operator in the parameters
       JavaScript knows to take that list of values you passed and turn it into an array.
    */
    //spread
const numbers = [1, 10, 99, -5];
console.log(Math.max(numbers)); //you will get an error because you can't pass an array type into the function
console.log(Math.max(1, 10, 99, -5)); //this is how the spread operator would input the items from the array, but with the syntax below.
console.log(Math.max(...numbers)); //=> it basically passes all the items in array as list. What it would look like if you removed the array brackets
    //rest
function makeArrays(...args: number) {
    return args;
}

//rest parameters & tuples
function printInfo(name: string, age: number) {
    console.log('My name is ' + name + ' and I am ' + age + ' years old!');
}

function printInfo2(...info: [string, number]) {
    console.log('My name is ' + info[0] + ' and I am ' + info[1] + ' years old!');
}

//Destructuring Arrays
const myHobbies = ["Cooking", "Programming"];
// const hobby1 = myHobbies[0];
// const hobby2 = myHobbies[1]; This is what destructuring does. It saves you from having to write extra lines.

const [hobby1, hobby2] = myHobbies;

//Destructuring Objects
const powerUserData = {userName: "Max", age: 27};
// const userName = powerUserData.userName; This is what destructuring does. It saves you from having to write extra lines.
// const age = powerUserData.age;

const {userName, age} = powerUserData;

//Template Literals
const myName = "Max";
const greeting = `Hey ${myName}`;

//Other ES6 Features
    //Symbols
    //Generators
